import os

from application.settings import BASE_DIR

# ================================================= #
# *************** mysql数据库 配置  *************** #
# ================================================= #
# 数据库 ENGINE ，默认演示使用 sqlite3 数据库，正式环境建议使用 mysql 数据库
# sqlite3 设置
# DATABASE_ENGINE = "django.db.backends.sqlite3"
# DATABASE_NAME = os.path.join(BASE_DIR, "db.sqlite3")

# 使用mysql时，改为此配置
DATABASE_ENGINE = "django.db.backends.mysql"
# DATABASE_NAME = 'lowcodepython' # mysql 时使用

DATABASE_HOST = os.getenv('mysqlhost', '127.0.0.1')
DATABASE_PORT = os.getenv('mysqlport', '3306')
DATABASE_USER = os.getenv('mysqluser', 'root')
DATABASE_PASSWORD = os.getenv('mysqlpassword', 'root')
DATABASE_NAME = os.getenv('mysqltablename', 'lowcodepython')




IS_DEMO = False #是否演示模式（演示模式只能查看无法保存、编辑、删除、新增）
IS_SINGLE_TOKEN = False #是否只允许单用户单一地点登录(只有一个人在线上)(默认多地点登录),只针对后台用户生效
ALLOW_FRONTEND = True#是否关闭前端API访问
FRONTEND_API_LIST = ['/api/app/','/api/xcx/','/api/h5/']#微服务前端接口前缀




# # 数据库地址 改为自己数据库地址
# DATABASE_HOST = "127.0.0.1"
# # # 数据库端口
# DATABASE_PORT = 3306
# # # 数据库用户名
# DATABASE_USER = "root"
# # # 数据库密码
# DATABASE_PASSWORD = "root"

# 表前缀
TABLE_PREFIX = "low_"
# ================================================= #
# ******** redis配置，无redis 可不进行配置  ******** #
# ================================================= #
# REDIS_PASSWORD = ''
# REDIS_HOST = '127.0.0.1'
# REDIS_URL = f'redis://:{REDIS_PASSWORD or ""}@{REDIS_HOST}:6380'
# ================================================= #
# ****************** 功能 启停  ******************* #
# ================================================= #
# DEBUG = True

DEBUG = os.getenv('DEBUG', True)

# 启动登录详细概略获取(通过调用api获取ip详细地址。如果是内网，关闭即可)
ENABLE_LOGIN_ANALYSIS_LOG = True
# 登录接口 /api/token/ 是否需要验证码认证，用于测试，正式环境建议取消
LOGIN_NO_CAPTCHA_AUTH = False
# ================================================= #
# ****************** 其他 配置  ******************* #
# ================================================= #

ALLOWED_HOSTS = ["*"]

# daphne启动命令
#daphne application.asgi:application -b 0.0.0.0 -p 8000


# redis 配置
REDIS_PASSWORD =  os.getenv('REDIS_PASSWORD', '123456')    # 如果没密码就为空
REDIS_HOST = os.getenv('REDIS_HOST', '127.0.0.1')
REDIS_URL = f'redis://:{REDIS_PASSWORD or ""}@{REDIS_HOST}:6379'


# 支持的脚本文件
Fileuffix=['py', 'jar','java','go']


# elasesearch配置

elastsearch_url = ["http://127.0.0.1:9200"]

# 时序数据库

# 时序数据库

token = "DVeB9-NyP2EQwSIr3wuMaaTzWNFgfOz-ViXccpcTSgw9dXtky5P9IS4onAM1sDo4FhIpdgfScJrulsYHfzfAdA=="
org = "dev_org"
infuxdb_url = "http://127.0.0.1:8086"