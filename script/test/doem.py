from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# You can generate an API token from the "API Tokens Tab" in the UI
token = "9_AiODfhGTHrq19UTcRNdNMx4X9aCD_WRu3Fkj9Cu4AGnHLN9OjUYNDyoHoZjnVHWWACNS6rZhNcO8CfTPQf1g=="
org = "org"
bucket = "storage"

client =  InfluxDBClient(url="http://10.226.59.90:8086", token=token, org=org)


# query = 'from(bucket: "localhostsystem") |> range(start: -1h) |> filter(fn: (r) => r["_measurement"] == "disk") |> last()'
# tables = client.query_api().query(query, org=org)
#
# results = []
# for table in tables:
#   for record in table.records:
#     print(record)
#     results.append((record.get_field(), record.get_value(),record.values.get("device"),record.values.get("host"),record.values.get("path")))
#
# print(results)



write_api = client.write_api(write_options=SYNCHRONOUS)
#
# data = "mem,host=dfdf id=26.43234543"
# write_api.write('testdata', org, data)


p = Point("mem2").tag("location", "Prague").tag("a",3).field("temp2", 25.3).field("temp3",26)
write_api.write(bucket='testdata', org=org, record=p)

# query_api = client.query_api()
# query = """
# from(bucket:"my-bucket")\
# |> range(start: -10m)\
# |> filter(fn:(r) => r._measurement == "my_measurement")\
# |> filter(fn: (r) => r.location == "Prague")\
# |> filter(fn:(r) => r._field == "temperature" )
# """