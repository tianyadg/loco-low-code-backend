#!/bin/python
# coding: utf-8
import logging


def logger():
    logging.basicConfig(format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s',
                        # filename=f'./{datetime.now().strftime("%Y-%m-%d-%H.%M.%S")}.txt',
                        level=logging.DEBUG,
                        filemode='a+')
    return logging



log = logger()
