from django.conf.urls import url
from django.urls import path

from dvadmin.knowledge.views import documents,book,documentsEditor
from rest_framework import routers

system_url = routers.SimpleRouter()

#
system_url.register(r'documents', documents.BasisViewSet)
system_url.register(r'book', book.BasisViewSet)
system_url.register(r'documentsEditor', documentsEditor.BasisViewSet)


urlpatterns = [
    # url(r'blocks', views.BlockView),
    # url(r'typeblock', views.LowDataTypedView),
    # url(r'assets', views.AssetsView),
    # # 用于获取登入页面
    # url(r'schemas', views.SchemasView),
    # url(r'route', views.RouteView),
    # path('block/', blocks.LoginLogViewSet.as_view({'get': 'list','post': 'create'})),
    # path('block/<int:pk>/', blocks.LoginLogViewSet.as_view({'get': 'retrieve','put': 'update'})),
    # path('datatyped/', datatyped.LowDataTypedViewSet.as_view({'get': 'list','post': 'create'})),
    # path('datatyped/<int:pk>/', datatyped.LowDataTypedViewSet.as_view({'get': 'retrieve','put': 'update'})),
    # path('assetsdata/', assets.AssetsViewSet.as_view({'get': 'list'})),
    # path('schema/<int:pk>/', schema.SchemaViewSet._viewas({'get': 'retrieve'})),
    # path('getassets/', assets.NoautoAssetsViewSet.as_view({'get': 'list'})),
]

urlpatterns += system_url.urls
