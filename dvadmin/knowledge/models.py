from django.db import models
from dvadmin.utils.models import CoreModel
from bs4 import BeautifulSoup

class Book(CoreModel):
    name = models.CharField(verbose_name="名称", max_length=200, null=True)
    cover = models.CharField(max_length=255, verbose_name="封面", null=True, blank=True, help_text="封面")
    label = models.CharField(verbose_name="标签", max_length=200, null=True)
    privately_status = models.IntegerField( default=0, verbose_name="发布状态", help_text="发布状态")
    sort = models.IntegerField(default=1, verbose_name="显示排序", null=True, blank=True, help_text="显示排序")
    typed = models.IntegerField(verbose_name="所属类别", null=True)
    preface = models.TextField(verbose_name="前言", null=True)
    tempid = models.IntegerField(verbose_name="临时ID", null=True, blank=True)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = "书籍"
        verbose_name_plural = verbose_name
        ordering = ("sort",)


class Documents(CoreModel):
    name = models.CharField(verbose_name="名称", max_length=200, null=True)
    sort = models.IntegerField(default=1, verbose_name="显示排序", null=True, blank=True, help_text="显示排序")
    parent = models.ForeignKey(
        to="Documents",
        on_delete=models.CASCADE,
        default=None,
        verbose_name="上级项目",
        db_constraint=False,
        null=True,
        blank=True,
        help_text="上级项目",
    )
    release_markdown = models.TextField(verbose_name="发布的md", null=True)
    release_html = models.TextField(verbose_name="发布的html", null=True)
    release_markdown_editor = models.TextField(verbose_name="编辑内容", null=True)
    tempid = models.IntegerField(verbose_name="临时ID", null=True, blank=True)
    release_text = models.TextField(verbose_name="发布的文本", null=True)
    vcnt = models.IntegerField( default=0, verbose_name="阅读次数", help_text="阅读次数")
    typed = models.IntegerField(verbose_name="所属类别", null=True)
    book = models.ForeignKey(
        to="Book",
        db_constraint=False,
        on_delete=models.CASCADE,
        verbose_name="关联书籍",
        help_text="关联书籍",)
    def __str__(self):
        return self.name

    def bookid(self):
        return self.book.id
    def edittree(self):
        return "({})".format(self.sort) + self.name
    class Meta:
        verbose_name = "文档"
        verbose_name_plural = verbose_name
        ordering = ("sort",)
    def save(self, *args, **kwargs):

        if self.release_html:
            test = str(BeautifulSoup(self.release_html, 'lxml').get_text())
            test = test.replace("*","")
            self.release_text = test
        # 学生表的插入操作
        super(Documents, self).save(*args, **kwargs)


class DocumentsEditor(CoreModel):
    release_markdown = models.TextField(verbose_name="内容能提供", null=True)
    tempid = models.IntegerField(verbose_name="临时ID", null=True, blank=True)

    documents = models.OneToOneField(
        to="Documents",
        db_constraint=False,
        on_delete=models.CASCADE,
        verbose_name="关联文档",
        help_text="关联文档",)
    def __str__(self):
        return self.id
    class Meta:
        verbose_name = "文档未发布"
        verbose_name_plural = verbose_name



