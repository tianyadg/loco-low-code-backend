
from dvadmin.knowledge.models import DocumentsEditor
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from rest_framework.decorators import action
from rest_framework import serializers
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse


class editorBasisSerializer(CustomModelSerializer):
    """
    区块-序列化器
    """
    class Meta:
        model = DocumentsEditor
        fields = "__all__"
        read_only_fields = ["id"]


class BasisViewSet(CustomModelViewSet):
    """
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = DocumentsEditor.objects.all()
    serializer_class = editorBasisSerializer
    extra_filter_backends = []
    permission_classes = []
