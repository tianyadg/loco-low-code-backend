# documents.py

from django_elasticsearch_dsl import Document,fields
from django_elasticsearch_dsl.registries import registry
from .models import Documents,Book


@registry.register_document
class BookDocument(Document):
    class Index:
        # Name of the Elasticsearch index
        name = 'documents'
        book = fields.TextField(attr="bookid")

        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}
    class Django:
        model = Documents # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            'name',
            'release_markdown',
            'release_html',
            'release_text',
            'create_datetime',
            'update_datetime',
        ]
