# -*- coding: utf-8 -*-

"""
@Remark: websocket的路由文件
"""
from django.urls import re_path

from . import consumers_test,consumers

websocket_urlpatterns = [
    re_path(r'^ws/webssh/$', consumers.TerminalConsumer.as_asgi()),
    re_path(r'^ws/websshtest/$', consumers_test.TerminalConsumer.as_asgi()),

]