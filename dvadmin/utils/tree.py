
def get_role_api_trees(data,
              key_column='id',
              parent_column='parent',
              child_column='children'):
    """
    :param data: 数据列表
    :param key_column: 主键字段，默认id
    :param parent_column: 父ID字段名，父ID默认从0开始
    :param child_column: 子列表字典名称
    :return: 树结构
    """
    data_dic = {}
    for d in data:
        data_dic[d.get(key_column)] = d  # 以自己的权限主键为键,以新构建的字典为值,构造新的字典
        d["type"] = "menu"

    data_tree_list = []  # 整个数据大列表
    for d_id, d_dic in data_dic.items():
        # if d_dic["p
        # arent"]:
        #     d_dic["value"] = d_dic["value"] + "_1"
        del d_dic['menuPermission']
        pid = d_dic.get(parent_column)  # 取每一个字典中的父id

        if not pid:  # 父id=0，就直接加入数据大列表

            data_tree_list.append(d_dic)

        else:  # 父id>0 就加入父id队对应的那个的节点列表
            try:  # 判断异常代表有子节点，增加子节点列表=[]
                data_dic[pid][child_column].append(d_dic)
            except KeyError:
                try:
                    data_dic[pid][child_column] = []
                    data_dic[pid][child_column].append(d_dic)
                except:
                    pass

    return data_tree_list
def get_role_trees(data,
              key_column='id',
              parent_column='parent',
              child_column='children'):
    """
    :param data: 数据列表
    :param key_column: 主键字段，默认id
    :param parent_column: 父ID字段名，父ID默认从0开始
    :param child_column: 子列表字典名称
    :return: 树结构
    """
    data_dic = {}
    for d in data:
        d["value"] = d["value"] + "_1"
        d["type"] = "menu"

        data_dic[d.get(key_column)] = d  # 以自己的权限主键为键,以新构建的字典为值,构造新的字典


    data_tree_list = []  # 整个数据大列表
    for d_id, d_dic in data_dic.items():


        pid = d_dic.get(parent_column,None)  # 取每一个字典中的父id
        if not pid:  # 父id=0，就直接加入数据大列表

            data_tree_list.append(d_dic)
        else:  # 父id>0 就加入父id队对应的那个的节点列表

            try:  # 判断异常代表有子节点，增加子节点列表=[]

                d_dic['children'] = d_dic['menuPermission']
                del d_dic['menuPermission']
                data_dic[pid][child_column].append(d_dic)
            except KeyError:
                try:
                    data_dic[pid][child_column] = []
                    data_dic[pid][child_column].append(d_dic)
                except:
                    pass

    return data_tree_list





def get_trees(data,
              key_column='id',
              parent_column='parent',
              child_column='children'):
    """
    :param data: 数据列表
    :param key_column: 主键字段，默认id
    :param parent_column: 父ID字段名，父ID默认从0开始
    :param child_column: 子列表字典名称
    :return: 树结构
    """
    data_dic = {}
    for d in data:
        data_dic[d.get(key_column)] = d  # 以自己的权限主键为键,以新构建的字典为值,构造新的字典

    data_tree_list = []  # 整个数据大列表
    for d_id, d_dic in data_dic.items():
        pid = d_dic.get(parent_column)  # 取每一个字典中的父id
        if not pid:  # 父id=0，就直接加入数据大列表
            data_tree_list.append(d_dic)

        else:  # 父id>0 就加入父id队对应的那个的节点列表
            try:  # 判断异常代表有子节点，增加子节点列表=[]
                data_dic[pid][child_column].append(d_dic)
            except KeyError:
                try:

                    data_dic[pid][child_column] = []
                    data_dic[pid][child_column].append(d_dic)
                except:
                    pass
    return data_tree_list


def ListTodict(data,key_column='id'):
    data_dic = {}
    for d in data:
        data_dic[d.get(key_column)] = d
    return data_dic