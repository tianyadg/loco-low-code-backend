from dvadmin.system.models import SystemConfig


def getAllConfig(key):
    obj = SystemConfig.objects.filter(parent__key=key).values_list('key', 'value')
    dictlist = {}
    for item in obj:
        dictlist[item[0]] = item[1]
    return dictlist

