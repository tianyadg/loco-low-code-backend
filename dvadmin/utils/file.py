# -*- coding: utf-8 -*-

"""
@author: xiangli
@contact: QQ:1638245306
@Created on: 2021/6/2 002 16:06
@Remark: 自定义异常处理
"""
import os
from application.settings import BASE_DIR
from conf.env import Fileuffix

base = str(BASE_DIR) +'/script'


def GetAllFile():
    """
    获取目录内文件内容
    """
    filelist = []
    for root, dirs, files in os.walk(base):
        for file_name in files:
            if file_name.split(".")[1] in Fileuffix:
                print(os.path.join(root, file_name))
                filelist.append(os.path.join(root, file_name).replace(str(BASE_DIR) +"/",''))

    return filelist



def GetFileData(path:str):
    """
    获取相关文件数据
    """
    with open(path) as f:
        data = f.read()
        return data