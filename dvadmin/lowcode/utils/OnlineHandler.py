
from abc import abstractmethod


class OnlineHandler:
    def __init__(self,obj,requestdata, id,test=False):
        self.obj = obj
        self.requestdata = requestdata
        self.userid = id

    @abstractmethod
    def sqlprocess(self):
        """
        sql处理
        """
        pass
    @abstractmethod
    def createCursor(self):
        """
        连接数据库，并执行sql
        """
        pass
    @abstractmethod
    def process(self):
        # 获取返回的数据进行处理
        pass
