from django.apps import AppConfig


class LowcodeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dvadmin.lowcode'
    verbose_name = "低代码管理"