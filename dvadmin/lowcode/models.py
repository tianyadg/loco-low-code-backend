from django.db import models
from django.utils.html import format_html
from dvadmin.utils.models import CoreModel, table_prefix

class LowDataTyped(CoreModel):
    """
    类型管理
    """
    unique = (
        (1, "顶级目录"),
        (2, "二级目录"),
        (3, "三级目录")

    )
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(verbose_name="名称", max_length=200, null=True)
    typed = models.IntegerField(verbose_name="类型", null=True, choices=unique)
    pid = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='LowDataTyped',
                            verbose_name='父ID')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "数据类型"
        verbose_name_plural = verbose_name


class Block(CoreModel):
    """
    区块管理
    """
    id = models.BigAutoField(primary_key=True)
    name =models.CharField(verbose_name="中文名称",max_length=200,null=True)
    screenshot = models.TextField(verbose_name="截图",null=True)
    schema = models.TextField(verbose_name="schema信息",null=True)
    blockTypeId = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='LowDataTyped',
                            verbose_name='类型')
    # def image_data(self):
    #     return format_html(
    #         '<img src="{}" width="100px"/>',
    #         self.screenshot,
    #     )
    # image_data.short_description = u'截图'
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = "区块管理"
        verbose_name_plural = verbose_name
        ordering = ("id",)



# class Route(models.Model):
#     """
#     报废
#     """
#     unique = (
#         (1, "目录"),
#         (2, "菜单")
#     )
#     id = models.BigAutoField(primary_key=True)
#     pids = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='Route',verbose_name='父ID')
#     locator =models.CharField(verbose_name="路由名称",max_length=200,null=True)
#     icon = models.CharField(verbose_name="ICON",max_length=20,null=True)
#     name =models.CharField(verbose_name="菜单名称",max_length=200,null=True)
#     typed = models.IntegerField(verbose_name="类型",null=True,choices=unique)
#     schemadata_id = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='SchemasDate', verbose_name='页面数据')
#     def __str__(self):
#         return self.name
#     class Meta:
#         verbose_name = "路由管理"
#         verbose_name_plural = verbose_name

# class SchemasDate(models.Model):
#     """
#
#     """
#     id = models.BigAutoField(primary_key=True)
#     page =models.CharField(verbose_name="页面名称",unique=True,max_length=200,null=True)
#     schema =models.TextField(verbose_name="页面数据",null=True)
#     def __str__(self):
#         return self.page
#     class Meta:
#         verbose_name = "页面数据"
#         verbose_name_plural = verbose_name
#


class Schema(CoreModel):
    """
    页面数据存储管理
    """
    unique = (
        (0, "登入"),
        (1, "无需登入")
    )
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(verbose_name="模板名称",max_length=200,null=True)
    schema = models.TextField(verbose_name="模板数据",null=True)
    menu_id = models.BigIntegerField(verbose_name="菜单ID",blank=True, null=True)
    is_login = models.IntegerField(verbose_name="是否登入",null=True,choices=unique,default=0)
    Typed = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='LowDataTyped', verbose_name='数据类型')
    def __str__(self):
        return str(self.name)
    class Meta:
        verbose_name = "模板"
        verbose_name_plural = verbose_name
        ordering = ("-update_datetime",)




class Assets(CoreModel):
    """
    物料管理
    """
    unique = (
        (1,"激活"),
        (0, "未激活")
    )
    id = models.BigAutoField(primary_key=True)
    data =models.TextField(verbose_name="物料数据",null=True)
    version = models.CharField(verbose_name="版本",max_length=20,null=True)
    is_use = models.IntegerField(verbose_name="是否可用",null=True,choices=unique,default=2)
    class Meta:
        verbose_name = "物料数据"
        verbose_name_plural = verbose_name


class Datasource(CoreModel):
    """
    数据源管理
    """
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(verbose_name="数据源名称",unique=True,max_length=200,null=True)
    datatype = models.IntegerField(default=0, verbose_name="数据库类型", help_text="数据库类型")
    host = models.CharField(verbose_name="数据库地址",max_length=200,null=True)
    port = models.IntegerField(verbose_name="数据库端口",null=True)
    database = models.CharField(verbose_name="数据库名称",max_length=200,null=True)
    user = models.CharField(verbose_name="账号",max_length=200,null=True,blank=True)
    status = models.IntegerField(default=1, verbose_name="状态", null=True, blank=True, help_text="是否发布")
    password = models.CharField(verbose_name="密码",max_length=200,null=True,blank=True)
    key = models.CharField(verbose_name="key",max_length=200,null=True,blank=True)
    class Meta:
        verbose_name = "数据源管理"
        verbose_name_plural = verbose_name

class OnlineApi(CoreModel):
    """
    在线接口管理生成
    """
    name = models.CharField(verbose_name="接口名称",unique=True,max_length=200,null=True)
    data_source = models.IntegerField(verbose_name="数据源类型",null=True,default=0)
    datasource = models.ForeignKey(Datasource, on_delete=models.deletion.SET_NULL,null=True, verbose_name='数据源')
    typed = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='LowDataTyped', verbose_name='数据类型')
    datatyped = models.SmallIntegerField(verbose_name="数据类型",null=True,default=1)
    # 1需登入，0 无需登入
    is_login = models.SmallIntegerField(verbose_name="数据类型",null=True,default=1)
    # 所属app
    app_id = models.IntegerField(verbose_name="所属应用",null=True,default=0)
    #增删改查
    methods = models.IntegerField(verbose_name="接口方法",null=True,default=0)
    sort = models.IntegerField(default=1, verbose_name="显示排序", help_text="显示排序")
    status = models.IntegerField(default=1, verbose_name="状态", null=True, blank=True, help_text="是否发布")
    cacheid = models.CharField(verbose_name="缓存ID",max_length=30,null=True)
    cache_type= models.IntegerField(default=0, verbose_name="缓存类型", null=True, blank=True, help_text="缓存类型")
    url = models.CharField(verbose_name="接口地址",max_length=30,unique=True)
    sql = models.TextField(verbose_name="sql", help_text="sql", null=True, blank=True)
    parameter = models.TextField(verbose_name="入参json数据", help_text="入参json数据", null=True, blank=True)
    class Meta:
        verbose_name = "在线接口管理"
        verbose_name_plural = verbose_name
        ordering = ("id",)


class apiManager(CoreModel):
    """
    接口存储管理，配合前端引擎使用
    """
    name = models.CharField(verbose_name="接口名称",unique=True,max_length=200,null=True)
    schema = models.TextField(verbose_name="接口数据",null=True)
    typed = models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='LowDataTyped', verbose_name='数据类型')

    class Meta:
        verbose_name = "前端接口管理"
        verbose_name_plural = verbose_name


class template(CoreModel):
    name = models.CharField(verbose_name="模板名称",max_length=200,null=True)
    schema = models.TextField(verbose_name="模板数据",null=True)
    typed = models.IntegerField(blank=True, null=True, verbose_name='类型')
    screenshot = models.TextField(verbose_name="截图",blank=True, null=True)
    sort = models.IntegerField(default=1, verbose_name="显示排序", help_text="显示排序")
    status = models.IntegerField(default=1, verbose_name="状态", null=True, blank=True, help_text="是否启用")
    class Meta:
        verbose_name = "模板管理"
        verbose_name_plural = verbose_name
        ordering = ("sort","update_datetime")



class students(CoreModel):
    name = models.CharField(verbose_name="学生名",max_length=200,null=True)
    height = models.IntegerField(verbose_name="身高",null=True)
    classes = models.CharField(verbose_name="班级",max_length=200,null=True)
    sort = models.IntegerField(default=1, verbose_name="显示排序", help_text="显示排序")
    status = models.IntegerField(default=1, verbose_name="状态", null=True, blank=True, help_text="是否启用")
    class Meta:
        verbose_name = "test"
        verbose_name_plural = verbose_name



class assetPlugin(CoreModel):
    name = models.CharField(verbose_name="中文名称",max_length=50,null=True)
    assetName = models.CharField(verbose_name="物料名称",max_length=50,null=True)
    assetVersion = models.CharField(verbose_name="物料版本",max_length=50,null=True)
    devPeople = models.CharField(verbose_name="开发人员",max_length=50,null=True)
    gitUrl = models.CharField(verbose_name="git地址",max_length=200,null=True)
    status = models.IntegerField(default=1, verbose_name="状态")
    typed = models.IntegerField(default=0, verbose_name="分类")
    cover = models.CharField(verbose_name="git地址",max_length=200,null=True)
    markdown = models.TextField(verbose_name="markdown",null=True, blank=True)
    class Meta:
        verbose_name = "物料管理"
        verbose_name_plural = verbose_name



