from django.conf.urls import url
from django.urls import path

from dvadmin.lowcode.views import views
from dvadmin.lowcode.views import blocks,datatyped,assets,schema,onlineapi,datasource,apiManager,students,template,assetsPlugin
from rest_framework import routers

system_url = routers.SimpleRouter()

#
system_url.register(r'asset', assets.AssetsViewSet)
system_url.register(r'datatype', datatyped.LowDataTypedViewSet)
system_url.register(r'block', blocks.BLockViewSet)
system_url.register(r'schemadata', schema.SchemaPerViewSet)
system_url.register(r'onlineapi', onlineapi.OnlineApiPerViewSet)
system_url.register(r'datasource', datasource.SchemaPerViewSet)
system_url.register(r'apimanager', apiManager.SchemaPerViewSet)
system_url.register(r'students', students.studentsrViewSet)
system_url.register(r'template', template.templateViewSet)
system_url.register(r'assetplugin', assetsPlugin.assetPluginViewSet)

urlpatterns = [
    url(r'blocks', views.BlockView),
    # 废弃
    url(r'typeblock', views.LowDataTypedView),
    # url(r'assets', views.AssetsView),
    # 用于获取登入页面
    url(r'schemas', views.SchemasView),
    # url(r'route', views.RouteView),
    # path('block/', blocks.LoginLogViewSet.as_view({'get': 'list','post': 'create'})),
    # path('block/<int:pk>/', blocks.LoginLogViewSet.as_view({'get': 'retrieve','put': 'update'})),
    # path('datatyped/', datatyped.LowDataTypedViewSet.as_view({'get': 'list','post': 'create'})),
    # path('datatyped/<int:pk>/', datatyped.LowDataTypedViewSet.as_view({'get': 'retrieve','put': 'update'})),
    # path('assetsdata/', assets.AssetsViewSet.as_view({'get': 'list'})),
    # path('schema/<int:pk>/', schema.SchemaViewSet.as_view({'get': 'retrieve'})),
    path('getassets/', assets.NoautoAssetsViewSet.as_view({'get': 'list'})),
    path('online/<str:pk>/', onlineapi.OnlinePerViewSet.as_view({'get': 'api'})),

]

urlpatterns += system_url.urls
