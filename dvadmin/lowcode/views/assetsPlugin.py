
from dvadmin.lowcode.models import assetPlugin
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse
from rest_framework.decorators import action
from django.db.models import Q

class assetPluginSerializer(CustomModelSerializer):
    """
    物料管理
    """
    class Meta:
        model = assetPlugin
        fields = "__all__"
        # read_only_fields = ["id"]


class assetPubileSerializer(CustomModelSerializer):
    """
    物料管理
    """
    class Meta:
        model = assetPlugin
        fields = "__all__"
        # read_only_fields = ["id"]
class assetPluginViewSet(CustomModelViewSet):
    """
    物料管理
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = assetPlugin.objects.all()
    serializer_class = assetPluginSerializer
    extra_filter_backends = []


    @action(methods=['GET'], detail=False, permission_classes=[])
    def pubilc(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True, request=request)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True, request=request)
        return SuccessResponse(data=serializer.data, msg="获取成功")
