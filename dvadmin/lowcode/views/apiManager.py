
from dvadmin.lowcode.models import apiManager,LowDataTyped
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from rest_framework.decorators import action
from dvadmin.utils.json_response import SuccessResponse
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse
from rest_framework import serializers


class apiManagerSerializer(CustomModelSerializer):
    """
    接口数据管理
    """

    class Meta:
        model = apiManager
        fields = "__all__"
        read_only_fields = ["id"]
class TreeSerializer(CustomModelSerializer):
    """
    前端菜单路由的简单序列化器
    """
    value = serializers.IntegerField(source="id")
    label = serializers.CharField(source="name")

    class Meta:
        model = apiManager
        fields = '__all__'
        fields = ('label','value','id','schema','typed','description')


class SchemaPerViewSet(CustomModelViewSet):
    """
    接口数据管理
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = apiManager.objects.all()
    serializer_class = apiManagerSerializer
    extra_filter_backends = []

    @action(methods=['GET'], detail=False, permission_classes=[])
    def tree(self, request):
        typed = request.query_params['typed']
        queryset = self.queryset.filter(typed_id=typed)
        queryset._prefetched_objects_cache = {}

        serializer = TreeSerializer(queryset, many=True, request=request)
        data = serializer.data
        return SuccessResponse(data=data, total=len(data), msg="获取成功")

