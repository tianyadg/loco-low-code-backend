
from dvadmin.lowcode.models import template
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse
from rest_framework.decorators import action


class templateSerializer(CustomModelSerializer):
    """
    区块-序列化器
    """
    class Meta:
        model = template
        fields = "__all__"
        

        # read_only_fields = ["id"]

class templateViewSet(CustomModelViewSet):
    """
    区块管理
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """

    queryset = template.objects.all()
    serializer_class = templateSerializer
    extra_filter_backends = []
    soft_delete = False

    @action(methods=['GET'], detail=False,permission_classes=[])
    def Plubiclist(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True, request=request)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True, request=request)
        return SuccessResponse(data=serializer.data, msg="获取成功")

    @action(methods=['GET'], detail=True,permission_classes=[])
    def Publicretrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return DetailResponse(data=serializer.data, msg="获取成功")