
from dvadmin.lowcode.models import students
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from rest_framework.decorators import action
from dvadmin.utils.json_response import SuccessResponse
from dvadmin.utils.json_response import SuccessResponse, ErrorResponse, DetailResponse


class studentsiSerializer(CustomModelSerializer):
    """
    区块-序列化器
    """

    class Meta:
        model = students
        fields = "__all__"
        read_only_fields = ["id"]
# class AssetsViewSet(CustomModelViewSet):
#     """
#     物料管理
#     list:查询
#     create:新增
#     update:修改
#     retrieve:单例
#     destroy:删除
#     """
#     queryset = OnlineApi.objects.all()
#     serializer_class = OnlineApiSerializer


class studentsrViewSet(CustomModelViewSet):
    """
    在线接口管理
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    # authentication_classes = []
    # permission_classes = []
    queryset = students.objects.all()
    serializer_class = studentsiSerializer
    extra_filter_backends = []
    permission_classes = []

