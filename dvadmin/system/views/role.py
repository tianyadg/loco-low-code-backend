# -*- coding: utf-8 -*-

"""
@author: 猿小天
@contact: QQ:1638245306
@Created on: 2021/6/3 003 0:30
@Remark: 角色管理
"""
from rest_framework import serializers
from rest_framework.decorators import action
import json
from dvadmin.system.models import Role, Menu,MenuButton,App
from dvadmin.system.views.dept import DeptSerializer
from dvadmin.system.views.menu import MenuSerializer
from dvadmin.system.views.menu_button import MenuButtonSerializer
from dvadmin.utils.json_response import SuccessResponse
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.validator import CustomUniqueValidator
from dvadmin.utils.viewset import CustomModelViewSet
from dvadmin.utils.tree import get_role_trees,get_role_api_trees
from dvadmin.utils.tree import get_trees

class RoleSerializer(CustomModelSerializer):
    """
    角色-序列化器
    """

    class Meta:
        model = Role
        fields = "__all__"
        read_only_fields = ["id"]


class RoleInitSerializer(CustomModelSerializer):
    """
    初始化获取数信息(用于生成初始化json文件)
    """

    class Meta:
        model = Role
        fields = ['name', 'key', 'sort', 'status', 'admin', 'data_range', 'remark',
                  'creator', 'dept_belong_id']
        read_only_fields = ["id"]
        extra_kwargs = {
            'creator': {'write_only': True},
            'dept_belong_id': {'write_only': True}
        }


class RoleCreateUpdateSerializer(CustomModelSerializer):
    """
    角色管理 创建/更新时的列化器
    """
    menu = MenuSerializer(many=True, read_only=True)
    dept = DeptSerializer(many=True, read_only=True)
    permission = MenuButtonSerializer(many=True, read_only=True)
    key = serializers.CharField(max_length=50,
                                validators=[CustomUniqueValidator(queryset=Role.objects.all(), message="权限字符必须唯一")])
    name = serializers.CharField(max_length=50, validators=[CustomUniqueValidator(queryset=Role.objects.all())])

    def validate(self, attrs: dict):
        return super().validate(attrs)

    def save(self, **kwargs):
        data = super().save(**kwargs)
        data.dept.set(self.initial_data.get('dept', []))
        data.menu.set(self.initial_data.get('menu', []))
        data.permission.set(self.initial_data.get('permission', []))
        return data

    class Meta:
        model = Role
        fields = '__all__'


class MenuPermissonSerializer(CustomModelSerializer):
    """
    菜单的按钮权限
    """
    menuPermission = MenuButtonSerializer(many=True, read_only=True)

    class Meta:
        model = Menu
        fields = '__all__'
class NewWebRouterSerializer(CustomModelSerializer):
    """
    前端菜单路由的简单序列化器
    """
    value = serializers.IntegerField(source="id")
    label = serializers.CharField(source="name")

    class Meta:
        model = Role
        fields = '__all__'
        fields = ('label', 'value', 'sort','id')
        # 'component_name', 'cache', 'visible', 'menuPermission','schema')
        # read_only_fields = ["id"]


class NewMenuButtonSerializer(CustomModelSerializer):
    """
    菜单按钮-序列化器
    """

    key = serializers.IntegerField(source="id")
    value = serializers.IntegerField(source="id")

    label = serializers.CharField(source="name")
    class Meta:
        model = MenuButton
        read_only_fields = ["id"]
        fields = ('label', 'key','id','value')
        # fields = '__all__'



class NewMenuPermissonSerializer(CustomModelSerializer):
    """
    菜单的按钮权限,反向一对多
    """
    key = serializers.IntegerField(source="id")
    label = serializers.CharField(source="name")
    value = serializers.CharField(source="id")

    class Meta:
        model = Menu
        fields = ('label', 'key','id','parent','menuPermission','value',"app")



class NewMenuApiPermissonSerializer(CustomModelSerializer):
    """
    菜单的按钮权限,反向一对多
    """
    menuPermission = NewMenuButtonSerializer(MenuButton,many=True, read_only=True)
    key = serializers.IntegerField(source="id")
    label = serializers.CharField(source="name")
    value = serializers.CharField(source="id")

    class Meta:
        model = Menu
        fields = ('label', 'key','id','parent','menuPermission','value','app')

class RoleViewSet(CustomModelViewSet):
    """
    角色管理接口
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    create_serializer_class = RoleCreateUpdateSerializer
    update_serializer_class = RoleCreateUpdateSerializer

    @action(methods=['GET'], detail=True, permission_classes=[])
    def roleId_get_menu(self, request, *args, **kwargs):
        """通过角色id获取该角色用于的菜单"""
        # instance = self.get_object()
        # queryset = instance.menu.all()
        queryset = Menu.objects.filter(status=1).all()
        serializer = MenuPermissonSerializer(queryset, many=True)
        return SuccessResponse(data=serializer.data)

    @action(methods=['GET'], detail=False, permission_classes=[])
    def new_roleId_get_menu(self, request, *args, **kwargs):
        """通过角色id获取该角色用于的菜单"""

        queryset = Menu.objects.all().filter(is_deleted=0).all()
        serializer = NewMenuPermissonSerializer(queryset, many=True)
        res = json.dumps(serializer.data, ensure_ascii=False)
        res_data_list = json.loads(res)
        data = get_role_api_trees(res_data_list)
        appdata = App.objects.all().filter(is_deleted=0)
        alldata = []
        for i in appdata:
            menu = []
            for k in data:
                if k['app']:
                    if k['app'] == i.id:
                        menu.append(k)
            redata = {
                "id":i.id,
                "value":str(i.id)+"_2",
                "type":"app",
                "label":i.appName,
                "key":str(i.id)+"_2",
                "children":menu
            }
            alldata.append(redata)
        return SuccessResponse(data=alldata)
    @action(methods=['GET'], detail=False, permission_classes=[])
    def new_roleId_get_menu_api(self, request, *args, **kwargs):
        """通过角色id获取该角色用于的菜单"""

        queryset = Menu.objects.all().filter(is_deleted=0)
        serializer = NewMenuApiPermissonSerializer(queryset, many=True)
        res = json.dumps(serializer.data, ensure_ascii=False)
        res_data_list = json.loads(res)
        data = get_role_trees(res_data_list)
        appdata = App.objects.all().filter(is_deleted=0)
        alldata = []
        for i in appdata:
            menu = []
            for k in data:
                if k['app']:
                    if k['app'] == i.id:
                        menu.append(k)
            redata = {
                "id": i.id,
                "value": str(i.id) + "_2",
                "type": "app",
                "label": i.appName,
                "key": str(i.id) + "_2",
                "children": menu
            }
            alldata.append(redata)
        return SuccessResponse(data=alldata)
    @action(methods=['GET'], detail=False, permission_classes=[])
    def treeRoutr(self, request):
        """部门树结构"""
        serializer = NewWebRouterSerializer(self.queryset.all(), many=True, request=request)
        data = serializer.data
        return SuccessResponse(data=data, total=len(data), msg="获取成功")
    @action(methods=['GET'], detail=False, permission_classes=[])
    def tree(self, request):
        """用于前端菜单树结构树"""
        serializer = NewWebRouterSerializer(self.queryset.all(), many=True, request=request)
        data = serializer.data
        return SuccessResponse(data=get_trees(data), total=len(data), msg="获取成功")

