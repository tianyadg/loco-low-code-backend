#!/bin/python
#coding: utf-8

# ------------------------------
# celery 任务文件
# ------------------------------

from application.celery import app
import subprocess
from application.settings import BASE_DIR

def __external_cmd(cmd, code="utf8"):
    process = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while process.poll() is None:
      line = process.stdout.readline()
      line = line.strip()
      if line:
        print(line.decode(code, 'ignore'))
    return "运行成功"


@app.task(bind=True)
def lytask_test(self):

    return "django-vue-lyadmin lycrontab running"


@app.task(bind=True)
def lytask_test_a_b(self,a:int,b:int):
    print(a,b)
    return a+b


import importlib


@app.task(bind=True)
def lytask_start_script(self,path,dictdata):
    """
    运行脚本功能
    """
    try:
        __external_cmd("python {}".format(path))
        return "Ok"
    except Exception as e:
        return e