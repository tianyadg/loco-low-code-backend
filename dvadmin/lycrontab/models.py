from django.db import models
from dvadmin.utils.models import CoreModel


class ScriptManager(CoreModel):
    name = models.CharField(verbose_name="名称", max_length=200, null=True)
    path = models.CharField(verbose_name="名称", max_length=200, null=True)
    typed = models.SmallIntegerField( verbose_name="类型", default=0, help_text="类型")
    class Meta:
        verbose_name = "脚本管理"
        verbose_name_plural = verbose_name
