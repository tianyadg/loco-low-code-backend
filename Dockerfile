FROM python:3.8
MAINTAINER limanman
LABEL version="1.0" tag="1"

ENV DEBUG False
ENV TZ "Asia/Shanghai"
ADD ./ /app

RUN pip install --upgrade pip
RUN pip3 install -r /app/requirements.txt -i https://mirrors.aliyun.com/pypi/simple


WORKDIR /app
CMD ["supervisord","-c","/app/supervisor.conf"]

EXPOSE 8000

